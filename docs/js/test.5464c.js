/*!
 * kui-vue v3.4.9
 *  Copyright 2017-present, kui-vue.
 *  All rights reserved.
 *  Docs: https://k-ui.cn
 * Author: chuchur@qq.com / www.chuchur.com
 *
 */
"use strict";(self.webpackChunkkui_vue=self.webpackChunkkui_vue||[]).push([[5043],{525:function(e,t,a){a.r(t),a.d(t,{default:function(){return s}});var r=function(){var e=this,t=e._self._c;return t("div",[e._v("\n  "+e._s(e.test)+"\n  "),t("DatePicker"),e._v(" "),t("DatePicker",{attrs:{placeholder:"小尺寸/Small","picker-size":"small"}}),e._v(" "),t("br"),e._v(" "),t("DatePicker",{attrs:{mode:"month",placeholder:"请选择月份"}}),e._v(" "),t("DatePicker",{attrs:{mode:"dateRange"}}),e._v(" "),t("DatePicker",{attrs:{mode:"dateTimeRange","picker-size":"small"},model:{value:e.test,callback:function(t){e.test=t},expression:"test"}})],1)};r._withStripped=!0;var l={data(){return{test:[]}}},s=(0,a(1900).Z)(l,r,[],!1,null,null,null).exports}}]);